Promise = require 'bluebird'
_ = require 'lodash'
request = Promise.promisify(require('request'))
moment = require 'moment'
murmurhash = require 'murmurhash'

config = require '../config'
redis = require '../redis'

check = (message) ->
  message.indexOf('ECONNREFUSED') != -1 or message == 'timeout'

getCache = (lookupId) ->
  return new Promise (resolve, reject) ->
    redis.get lookupId, (err, results) ->
      return reject err if err
      return resolve results

setCache = (lookupId, results) ->
  return new Promise (resolve, reject) ->
    redis.setex lookupId, config.REDIS_CACHE_INTERVAL, results, (err) ->
      return reject err if err
      return resolve results

getEvents = (city, radius, date) ->

  bandsintownApi = (city, radius, date) ->
    params = "location=#{city}&radius=#{radius}&date=#{date}"
    return config.BANDSINTOWN + params

  return request bandsintownApi(city, radius, date)
  .spread (res, results) ->
    if res.statusCode != 200
      return throw new Error('Unexpected status code: ' + res.statusCode)
    results = JSON.parse(results)
    return false unless !results or !results?.venue or !result?.artists

    events = _.map results, (result) ->
      eventInfo=
        eventInfo:
          artists: _.map result.artists, (artist) -> return artist.name
          datatime: result.datetime
          ticket_status: result.ticket_status
          city: result.venue.city
          name: result.venue.name
          latitude: result.venue.latitude
          longitude: result.venue.longitude
      return eventInfo
    return events

getWeather = (latitude, longitude, date) ->
  openweathermapApi = (latitude, longitude, date) ->
    startDate = new Date(date).getTime()
    endDate = moment().add(24, 'hour')
    prms = "lat=#{latitude}&lon=#{longitude}&start=#{startDate}&end=#{endDate}"
    return config.OPENWEATHERMAP + prms

  return request openweathermapApi(latitude, longitude, date)
  .spread (res, results) ->
    if res.statusCode != 200
      return throw new Error('Unexpected status code: ' + res.statusCode)
    results = JSON.parse(results)

    return false unless results
    return {openweathermap_error: results.message} if results.cod is '404'
    return false unless results.list[0]

    main = results.list[0].main
    doc =
      datetime: date
      temp: main.temp
      temp_min: main.temp_min
      temp_max: main.temp_max
      humidity: main.humidity
      description: results.list[0].weather[0].description
    return doc

getSpotifyTopThreeTracks = (name) ->
  artistsCall = "https://api.spotify.com/v1/search?q=#{name}&type=artist"
  return request artistsCall
  .spread (res, results) ->
    if res.statusCode != 200
      return throw new Error('Unexpected status code: ' + res.statusCode)

    results = JSON.parse(results)
    return false unless results


    spotifyId = _.last(results.artists.items).id
    return null unless spotifyId

    # TODO: selecting top three only in DE, make more elastic in future
    spotifyApi = 'https://api.spotify.com/v1/artists/'
    spotifyCall = "#{spotifyApi}#{spotifyId}/top-tracks?country=DE"
    return request spotifyCall
    .spread (_res, _results) ->
      if res.statusCode != 200
        return throw new Error('Unexpected status code: ' + res.statusCode)
      _results = JSON.parse(_results)?.tracks
      topThree = _results.slice(0, 3)
      tracks = _.map topThree, (top) -> return url: top.external_urls?.spotify
      doc =
        name: name
        tracks: tracks
      return doc

module.exports.get = (req, res, next) ->
  {city, radius, date, limit} = req.params
  lookupId = murmurhash.v3(city + radius + date)

  return getCache lookupId
  .then (cachedEvents) ->
    if cachedEvents
      return res.json cachedEvents.slice(0, limit)

    return getEvents(city, radius, date)
    .then (events) ->
      Promise.each events, (event) ->
        {latitude, longitude} = event.eventInfo
        return getWeather(latitude, longitude, date)
        .then (_weather) ->
          event.weather = _weather
          return

    .then (events) ->
      Promise.each events, (event) ->
        artists = _.clone event.eventInfo.artists
        delete event.eventInfo.artists

        for artist in artists
          return getSpotifyTopThreeTracks artist
          .then (tracks) ->
            event.eventInfo.artists = tracks

    .then (events) ->
      # send json without waiting for setCache
      res.json events.slice(0, limit)

      setCache lookupId, events
        .catch (err) ->
          log.error err, 'setCache failed'
  .catch next
