redis = require 'redis'
parseRedisUrl = require('parse-redis-url')(redis)
config = require './config'

options = parseRedisUrl.parse config.REDIS_URL
delete options.password if options.password is 'redis'
options.auth_pass = options.password if options.password?
options.database ?= 0
options.retry_delay = config.redisRetryDelay
options.retry_max_delay = config.redisRetryMaxDelay
options.detect_buffers = true
options.no_ready_check = true

redisClient = redis.createClient options.port, options.host, options

if process.env.NODE_ENV == 'test'
  redisClient.unref()

redisClient.on 'error', (err) ->
  if err.message.indexOf('ECONNREFUSED') != -1
  else
    if redisClient.listeners('error').length <= 1
      return console.error {err}, 'REDIS CLIENT ERROR'

redisClient.auth options.auth_pass, (err) ->
  console.warn {err}, 'Error with redis.auth' if err?
redisClient.select options.database, (err) ->
  console.warn {err}, 'Error selecting database' if err?

module.exports.setex = (key, seconds, value, callback) ->
  redisClient.setex key, seconds, JSON.stringify(value), callback

module.exports.get = (key, callback) ->
  redisClient.get key, (err, response) ->
    return callback err if err?
    try
      response = JSON.parse(response)
    catch err
      return callback err
    callback null, response
