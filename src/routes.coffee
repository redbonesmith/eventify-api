express = require 'express'
infoService = require './services/event_info'

router = express.Router()

router.get '/city/:city/radius/:radius/date/:date/limit/:limit', infoService.get

module.exports = router
