require('dotenv').config({ silent: true })

config = module.exports
config.PORT = parseInt(process.env.PORT or '18000', 10)

config.REDIS_URL = process.env.REDIS_URL
config.REDIS_PORT = process.env.REDIS_PORT
config.REDIS_CACHE_INTERVAL = process.env.REDIS_CACHE_INTERVAL
config.redisErrorMessage = 'Redis server is unavailable'
config.redisRetryDelay = parseInt(process.env.REDIS_RETRY_DELAY or '100', 10)
config.redisRetryMaxDelay = parseInt(process.env.REDIS_RETRY_MAX_DELAY or '5000', 10)

config.BANDSINTOWN = "http://api.bandsintown.com/events/search?format=json&"

OPENWEATHERMAP_API_KEY = process.env.OPENWEATHERMAP_API_KEY
config.OPENWEATHERMAP = "http://api.openweathermap.org/data/2.5/forecast?units=metric&type=hour&cnt=1&APPID=#{OPENWEATHERMAP_API_KEY}&"

SPOTIFY_API_KEY = process.env.SPOTIFY_API_KEY
config.SPOTIFY = "https://api.spotify.com/v1/artists/#{SPOTIFY_API_KEY}/top-tracks?country=DE"
