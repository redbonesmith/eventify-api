express = require('express')
bodyParser = require 'body-parser'
path = require('path')
routes = require './routes'
cors = require 'cors'
app = express()

app.set 'trust proxy', ['loopback', 'linklocal', 'uniquelocal']

app.use bodyParser.urlencoded(extended: true)
app.use bodyParser.json()


app.use cors(origin: true)
app.use routes
app.use express.static(path.join(__dirname, '../public'))

env = process.env.NODE_ENV || 'development'
app.locals.ENV = env
app.locals.ENV_DEVELOPMENT = env == 'development'

module.exports = app
