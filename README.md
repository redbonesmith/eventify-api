[ ![Codeship Status for redbonesmith/eventify-api](https://codeship.com/projects/937a6be0-f9ad-0133-1b0c-0e6ed700efb9/status?branch=master)](https://codeship.com/projects/151301)

## Usage: ##
First of all create `.env` file which contains all env variables you need
example `.env` file:
```sh
PORT=18000
REDIS_URL=redis://redis:somePassword@localhost/1
REDIS_PORT=6379
REDIS_CACHE_INTERVAL=900
REDIS_RETRY_DELAY=100
REDIS_RETRY_MAX_DELAY=5000
OPENWEATHERMAP_API_KEY=/openweathermap api key/
SPOTIFY_API_KEY=/spotify api key/
```

## Start: ##
```sh
$ brew install redis # install redis
$ redis-server # run redis-server
$ npm install
$ ./bin/server
```

## About: ##
get concerts or festivals data alongside with weather and artist info
also providing `ticket_status` if available
using:

* http://openweathermap.org/api
* spotify api
* bandsintown api
* caching with redis
* coffee-script (TODO: get rid of coffee-script)
* promises

# request sample

```sh
# http get http://localhost:18000/city/kiev/radius/50/date/2015-12-12/limit/100
```

# response example

```json
[
    {
        "eventInfo": {
            "artists": {
                "name": "KINREE",
                "tracks": [
                    {
                        "url": "https://open.spotify.com/track/6RvPGgmbPSYGleM4m1L4oo"
                    },
                    {
                        "url": "https://open.spotify.com/track/2GPuoI5kD0D8qrbwIcL9PS"
                    },
                    {
                        "url": "https://open.spotify.com/track/5FmvftqlrJLkIiHZ68leVt"
                    }
                ]
            },
            "city": "Kiev",
            "datatime": "2015-12-12T23:00:37",
            "latitude": 50.43935,
            "longitude": 30.43477,
            "name": "Forsage",
            "ticket_status": "unavailable"
        },
        "weather": {
            "datetime": "2015-12-12",
            "description": "sky is clear",
            "humidity": 75,
            "temp": 8.12,
            "temp_max": 8.12,
            "temp_min": 6.09
        }
    },
    {
        "eventInfo": {
            "artists": {
                "name": "So Inagawa",
                "tracks": [
                    {
                        "url": "https://open.spotify.com/track/49mPGfqZfL7BRaKsuSIWCt"
                    },
                    {
                        "url": "https://open.spotify.com/track/3GSq4f8GkSd82OeHQcUZLy"
                    },
                    {
                        "url": "https://open.spotify.com/track/2d7cYXtCoqYMZeJTeCAYpU"
                    }
                ]
            },
            "city": "Kiev",
            "datatime": "2015-12-12T23:59:00",
            "latitude": 50.466403,
            "longitude": 30.500275,
            "name": "Closer",
            "ticket_status": "available"
        },
        "weather": {
            "datetime": "2015-12-12",
            "description": "sky is clear",
            "humidity": 75,
            "temp": 8.12,
            "temp_max": 8.12,
            "temp_min": 6.09
        }
    }
]
```

## TEST: ##
* install coffeetape :
```sh
$ npm install -g coffeetape
```
* run test:
```sh
$ npm test
```