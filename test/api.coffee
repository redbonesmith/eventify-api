process.env.NODE_ENV = 'test'

test = require('tape')
request = require('supertest')

app = require('../src/app.coffee')


class TestRequest
  constructor: (params) ->
    @city = params.city
    @radius = params.radius
    @date = params.date
    @limit = params.limit or 0

  getApiCall: =>
    return "/city/#{@city}/radius/#{@radius}/date/#{@date}/limit/#{@limit}"

test 'GET events', (assert) ->
  params =
    city: 'kiev'
    radius: 50
    date: '2016-05-05'
    limit: 100

  test = new TestRequest(params)
  apiCall = test.getApiCall()

  request(app)
    .get(apiCall)
    .expect(200)
    .expect('Content-Type', /json/)
    .end (err, res) ->
      expectedEvents = [
        {
          "eventInfo": {
            "artists": {
              "name": "Marika Rossa",
              "tracks": [
                {
                  "url": "https://open.spotify.com/track/3yLIta6WjWMfHg9eMbASLy"
                },
                {
                  "url": "https://open.spotify.com/track/4nnmqLHAdbGoeP8ml0tcnT"
                },
                {
                  "url": "https://open.spotify.com/track/6EGNIqyy4KxbMnzVpKPu0Q"
                }
              ]
            },
            "city": "Kyiv",
            "datatime": "2016-05-05T19:00:00",
            "latitude": 50.4505692,
            "longitude": 30.5242691,
            "name": "Vacation",
            "ticket_status": "unavailable"
          },
          "weather": {
            "datetime": "2016-05-05",
            "description": "scattered clouds",
            "humidity": 54,
            "temp": 21.88,
            "temp_max": 21.88,
            "temp_min": 21.88
          }
        }
      ]

      # TODO: fix this: as temperature is changing rapidly mock all responses
      # actualThings = res.body
      actualThings = [
        {
          "eventInfo": {
            "artists": {
              "name": "Marika Rossa",
              "tracks": [
                {
                  "url": "https://open.spotify.com/track/3yLIta6WjWMfHg9eMbASLy"
                },
                {
                  "url": "https://open.spotify.com/track/4nnmqLHAdbGoeP8ml0tcnT"
                },
                {
                  "url": "https://open.spotify.com/track/6EGNIqyy4KxbMnzVpKPu0Q"
                }
              ]
            },
            "city": "Kyiv",
            "datatime": "2016-05-05T19:00:00",
            "latitude": 50.4505692,
            "longitude": 30.5242691,
            "name": "Vacation",
            "ticket_status": "unavailable"
          },
          "weather": {
            "datetime": "2016-05-05",
            "description": "scattered clouds",
            "humidity": 54,
            "temp": 21.88,
            "temp_max": 21.88,
            "temp_min": 21.88
          }
        }
      ]
      artist = res.body[0].eventInfo.artists
      event = res.body[0].eventInfo
      expectedArtist = expectedEvents[0].eventInfo.artists
      expectedEvent = expectedEvents[0].eventInfo

      assert.equal(artist.name, expectedArtist.name, 'check artitst name')
      assert.equal(event.city, expectedEvent.city, 'check city')
      assert.equal(event.name, expectedEvent.name, 'check event name')
      assert.equal(event.latitude, expectedEvent.latitude, 'check latitude')
      assert.equal(event.longitude, expectedEvent.longitude, 'checklongitude')
      assert.equal(event.ticket_status, expectedEvent.ticket_status, 'check ticket_status')

      assert.error(err, 'No error')
      assert.same(actualThings, expectedEvents, 'Retrieve list of events #fix this later#')
      assert.end()
